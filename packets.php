<?php
include_once("header.php");
?>
<br>
<br>
<div class="centr">

<div class="row">
  <div class="col-sm-3">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-header " ><h5>1 Місяць</h5></div>
  <div class="card-body">
    <h6 class="card-title">- дозволяє додати до 25 продуктів</h6>
    <br>
    <br>
    <br>
    <h5 class="card-text ">100 $</h5>
    <br>
    <a href="afterpayment.php" class="btn btn-light">Купити</a>
  </div>
</div>
</div>
<div class="col-sm-3">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-header " ><h5>3 Місяця</h5></div>
  <div class="card-body">
    <h6 class="card-title">- дозволяє додати до 100 продуктів</h6>
    <br>
    <br>
    <br>
    <h5 class="card-text ">300 $</h5>
    <br>
    <a href="afterpayment.php" class="btn btn-light">Купити</a>
  </div>
</div>
</div>
<div class="col-sm-3">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-header " ><h5>6 Місяців</h5></div>
  <div class="card-body">
    <h6 class="card-title">- дозволяє додати до 500 продуктів</h6>
    <br>
    <br>
    <br>
    <h5 class="card-text ">500 $</h5>
    <br>
    <a href="afterpayment.php" class="btn btn-light">Купити</a>
  </div>
</div>
</div>
<div class="col-sm-3">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-header " ><h5>Рік</h5></div>
  <div class="card-body">
    <h6 class="card-title">- дозволяє додати необмежену кількість продуктів</h6>
    <br>
    <h5 class="card-text ">1000 $</h5>
    <br>
    <a href="afterpayment.php" class="btn btn-light">Купити</a>
  </div>
</div>
</div>

</div>