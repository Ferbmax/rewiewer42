<?php
include_once("header.php");
?>
    <br>
    <br>

<div class="centr">

<div class="row">
  <div class="col-sm-6">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-body">
    <h4 class="card-title">Увійдіть, якщо ви вже користувалися сервісом</h4>
    <br>
    <br>
    <a href="autorization.php" class="btn btn-light"><h4>ВХІД</h4></a>
    <br>

  </div>
</div>
</div>

<div class="col-sm-6">
<div class="card text-white bg-danger mb-3 text_center" >
  <div class="card-body">
    <h4 class="card-title">Зареєструйтесь, якщо ви вперше купуєте пакет</h4>
    <br>
    <br>
    <a href="registration.php" class="btn btn-light"><h4>РЕЄСТРАЦІЯ</h4></a>
    <br>

  </div>
</div>
</div>


</div>



<?php
include_once("footer.php");
?>