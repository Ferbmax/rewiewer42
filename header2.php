<?php
  include_once("./connection.php"); 
	session_start();	
	if(!isset($_SESSION["session_username"])){
		header("Location: ../index.php");

	}
?> 



<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="../css/styles.css" rel="stylesheet" >

</head>
<body>

<?php

?>
<div class="centr">
<img src="../img/title.png" width="790" alt="...">
<div class="container">
<div class="row ">
    <div class="col-3 cent">
    <a class="btn btn-warning  btn-lg btn-block cent" href="../packets.php" role="button"><h6>Кабінет</h6></a>
    </div>
    <div class="col-3 cent">
    <a class="btn btn-warning  btn-lg btn-block cent col-12" href="../addprod.php" role="button"><h6>Додати продукт</h6></a>
    </div>
    
    <div class="col-3 cent">
    <a class="btn btn-warning  btn-lg btn-block cent" href="../myprod.php" role="button"><h6>Мої продукти</h6></a>
        
    </div>
    <div class="col-3 cent">
    <a class="btn btn-warning  btn-lg btn-block cent" href="../logout.php" role="button"><h6>Вихід</h6></a>    
    </div>
  </div>

</div>
</div>